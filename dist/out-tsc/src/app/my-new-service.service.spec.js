import { TestBed, inject } from '@angular/core/testing';
import { MyNewServiceService } from './my-new-service.service';
describe('MyNewServiceService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [MyNewServiceService]
        });
    });
    it('should be created', inject([MyNewServiceService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=my-new-service.service.spec.js.map