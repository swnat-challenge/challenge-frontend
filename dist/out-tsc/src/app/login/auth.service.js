var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/observable/of";
import "rxjs/add/operator/do";
import "rxjs/add/operator/delay";
var AuthService = /** @class */ (function () {
    function AuthService() {
        this.isLoggedIn = false;
        //Tabla de usuarios mock
        this.users = ["marco", "natura", "fede", "test"];
    }
    AuthService.prototype.ngOnInit = function () {
        this.users = ["marco", "natura", "fede", "test"];
    };
    AuthService.prototype.isLogged = function () {
        console.log("isLogged: " + this.isLoggedIn);
        return this.isLoggedIn;
    };
    AuthService.prototype.login = function (user) {
        var _this = this;
        return Observable.of(true)
            .delay(400)
            .do(function (val) {
            _this.isLoggedIn = false;
            if (_this.users.indexOf(user) !== -1) {
                _this.isLoggedIn = true;
            }
        });
    };
    AuthService.prototype.logout = function () {
        this.isLoggedIn = false;
    };
    AuthService = __decorate([
        Injectable()
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map