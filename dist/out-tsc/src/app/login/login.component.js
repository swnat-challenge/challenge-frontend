var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.msgs = [];
    }
    LoginComponent.prototype.setMessage = function () {
        var autenticado = this.authService.isLoggedIn;
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        if (this.user === '' || this.user === null) {
            this.msgs.push({ severity: 'warn', summary: 'Debe ingresar usuario', detail: '' });
            return;
        }
        this.authService.login(this.user).subscribe(function () {
            var autenticado = _this.authService.isLoggedIn;
            if (_this.authService.isLoggedIn) {
                _this.msgs = [];
                // Get the redirect URL from our auth service
                // If no redirect has been set, use the default
                var redirect = _this.authService.redirectUrl ? _this.authService.redirectUrl : '/menu';
                // Redirect the user
                _this.router.navigate([redirect]);
            }
            else {
                _this.msgs.push({ severity: 'error', summary: 'No existe usuario', detail: '' });
            }
        });
    };
    LoginComponent.prototype.logout = function () {
        this.msgs = [];
        this.authService.logout();
    };
    LoginComponent = __decorate([
        Component({
            selector: 'app-login-component',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [AuthService, Router])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map