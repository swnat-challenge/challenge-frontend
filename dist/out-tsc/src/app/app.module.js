var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { DataTableModule } from "primeng/primeng";
import { InputTextModule } from "primeng/primeng";
import { DropdownModule } from "primeng/primeng";
import { DataListModule } from "primeng/primeng";
import { SharedModule } from "primeng/primeng";
import { ButtonModule } from "primeng/primeng";
import { GrowlModule } from "primeng/primeng";
import { HttpClientModule } from "@angular/common/http";
import { LoginComponent } from "./login/login.component";
import { AppRoutingModule } from "./app-routing.module";
import { MenuComponent } from "./menu/menu.component";
import { NotFoundComponent } from "./not-found.component";
import { ListarLocalesComponent } from "./locales/listar-locales.component";
import { AuthService } from "./login/auth.service";
import { MessagesModule } from "primeng/primeng";
import { DialogModule } from "primeng/primeng";
import { CheckboxModule } from "primeng/primeng";
import { TableModule } from "primeng/table";
import { ChartModule } from "primeng/chart";
import { InfoComponent } from './info/info.component';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                LoginComponent,
                MenuComponent,
                ListarLocalesComponent,
                NotFoundComponent,
                InfoComponent
            ],
            imports: [
                BrowserModule,
                FormsModule,
                HttpModule,
                HttpClientModule,
                AppRoutingModule,
                BrowserAnimationsModule,
                RouterModule,
                DataTableModule,
                InputTextModule,
                DropdownModule,
                DataListModule,
                SharedModule,
                ButtonModule,
                GrowlModule,
                MessagesModule,
                DialogModule,
                CheckboxModule,
                TableModule,
                ChartModule
            ],
            providers: [AuthService],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map