import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { DataTableModule, TableBody } from "primeng/primeng";
import { InputTextModule } from "primeng/primeng";
import { DropdownModule } from "primeng/primeng";
import { DataListModule } from "primeng/primeng";
import { SharedModule } from "primeng/primeng";
import { ButtonModule } from "primeng/primeng";
import { GrowlModule } from "primeng/primeng";
import { HttpClientModule } from "@angular/common/http";
import { LoginComponent } from "./login/login.component";
import { AppRoutingModule } from "./app-routing.module";
import { MenuComponent } from "./menu/menu.component";
import { NotFoundComponent } from "./not-found.component";
import { NurseComponent } from "./nurse/nurse.component";
import { AuthService } from "./login/auth.service";
import { MessagesModule } from "primeng/primeng";
import { DialogModule } from "primeng/primeng";
import { CheckboxModule } from "primeng/primeng";
import { TableModule } from "primeng/table";
import { ChartModule } from "primeng/chart";
import { InfoComponent } from "./info/info.component";
import { PatientComponent } from './patient/patient.component';
import { AnswersComponent } from './answers/answers.component';
import { AlgorithmsComponent } from './algorithms/algorithms.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    NurseComponent,
    NotFoundComponent,
    InfoComponent,
    PatientComponent,
    AnswersComponent,
    AlgorithmsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    DataTableModule,
    InputTextModule,
    DropdownModule,
    DataListModule,
    SharedModule,
    ButtonModule,
    GrowlModule,
    MessagesModule,
    DialogModule,
    CheckboxModule,
    TableModule,
    ChartModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
