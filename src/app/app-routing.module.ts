import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

// components
import { LoginComponent } from "./login/login.component";
import { InfoComponent } from "./info/info.component";
import { MenuComponent } from "./menu/menu.component";
import { NurseComponent } from "./nurse/nurse.component";
import { PatientComponent } from "./patient/patient.component";
import { AnswersComponent } from "./answers/answers.component";
import { AlgorithmsComponent } from "./algorithms/algorithms.component";
import { NotFoundComponent } from "./not-found.component";
import { AuthGuard } from "./login/auth.guard";
import { AuthService } from "./login/auth.service";
const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  {
    path: "menu",
    component: MenuComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "nurse", component: NurseComponent },
      { path: "patient", component: PatientComponent },
      { path: "info", component: InfoComponent },
      { path: "answer", component: AnswersComponent },
      { path: "algorithms", component: AlgorithmsComponent }
    ]
  },
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: [AuthGuard, AuthService]
})
export class AppRoutingModule {}
