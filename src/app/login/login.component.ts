import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";
import { Message } from "primeng/components/common/api";

@Component({
  selector: "app-login-component",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  user: string;
  pass: string;
  msgs = [];
  constructor(public authService: AuthService, public router: Router) {}

  setMessage() {
    let autenticado: boolean = this.authService.isLoggedIn;
  }

  login() {
    if (this.user === "" || this.user === null) {
      this.msgs.push({
        severity: "warn",
        summary: "Debe ingresar usuario",
        detail: ""
      });
      return;
    }
    this.authService.login(this.user).subscribe(() => {
      let autenticado: boolean = this.authService.isLoggedIn;
      if (this.authService.isLoggedIn) {
        this.msgs = [];
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        let redirect = this.authService.redirectUrl
          ? this.authService.redirectUrl
          : "/menu/info";
        // Redirect the user
        this.router.navigate([redirect]);
      } else {
        this.msgs.push({
          severity: "error",
          summary: "User Not Found",
          detail: ""
        });
      }
    });
  }

  logout() {
    this.msgs = [];
    this.authService.logout();
  }
}
