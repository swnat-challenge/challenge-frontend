import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';

import { AuthService} from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private login: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('CanActivate. Route: ' + route + ' state: ' + state);
    let url: string = state.url;
    return this.checkLogin(url);
  }

  /**canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  } */

  checkLogin(url: string): boolean {
    if (this.login.isLogged()) { return true; }
    this.router.navigate(['/login']);
    return false;
  }
}