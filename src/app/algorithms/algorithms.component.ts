import { Component, OnInit } from "@angular/core";
import { Button } from "primeng/primeng";
import { MyNewServiceService } from "./../my-new-service.service";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { MessageService } from "primeng/components/common/messageservice";
@Component({
  selector: "app-algorithms",
  templateUrl: "./algorithms.component.html",
  styleUrls: ["./algorithms.component.css"],
  providers: [MessageService]
})
export class AlgorithmsComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  url = "http://localhost:8080";

  number: number;

  serie: any[];

  series: string;

  ngOnInit() {}

  getSeries() {
    if (this.number == null || this.number == undefined) {
      this.messageService.add({
        severity: "error",
        summary: "Put a Number..",
        detail: ""
      });
    }
    this.http.get(this.url + "/fibonacci/" + this.number).subscribe(
      data => {
        this.series = null;
        this.parseData(data);
        console.log(data);
      },
      error => {
        console.log("error " + error);
      }
    );
  }
  parseData(data: any) {
    data.forEach(element => {
      if (element != null) {
        if (this.series == null) {
          this.series = element;
        } else {
          this.series += " - " + element;
        }
      }
    });
  }
}
