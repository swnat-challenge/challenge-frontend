import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import { Observable } from "rxjs";
import { Response } from "@angular/http";
import { URLSearchParams } from "@angular/http";
import "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { environment } from "../environments/environment";

@Injectable()
export class MyNewServiceService {
  private url = environment.service_uri + "/local/all";
  private headers = new Headers({ "Content-Type": "text/plain" });
  constructor(private http: Http) {}

  listar(): Observable<any> {
    return this.http.get(this.url, { headers: this.headers });
  }
}
